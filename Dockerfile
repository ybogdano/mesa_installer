# 20.04, focal-20231003, focal
# 22.04, jammy-20231004, jammy, latest * (Default)
# 23.04, lunar-20231004, lunar
# 23.10, mantic-20231011, mantic, rolling



# Set none accessible variables 
ARG DEBIAN_FRONTEND=noninteractive \
    DEFAULT_CODE_VERSION=22.04 

FROM ubuntu:${DEFAULT_CODE_VERSION} as base

# define work directory
WORKDIR /src

# Set all accessible variables
ENV MESA_REPO=https://gitlab.freedesktop.org/mesa/mesa.git \
    MESA_SHA='' \
    MESA_BUILD_PATH=./build \
    MESA_INSTALL_PATH=/opt/mesa

# Build dependency for MESA
RUN apt-get update && apt-get install -y --show-progress \
    bison \
    build-essential \
    ccache \
    cmake \
    flex \
    g++-multilib \
    gcc-multilib \
    git \
    glslang-tools \
    libc6 \
    libc6-dev \
    libcaca0 \
    libdrm-dev \
    libdrm2 \
    libedit-dev \
    libegl-mesa0 \
    libegl1-mesa-dev \
    libelf-dev \
    libepoxy-dev \
    libexpat1-dev \
    libgbm-dev \
    libgbm1 \
    libgl1-mesa-dev \
    libgl1-mesa-dri \
    libglapi-mesa \
    libglu1-mesa \
    libglu1-mesa-dev \
    libglx-mesa0 \
    libgtk-3-dev \
    libicu-dev \
    libjpeg-dev \
    libncurses-dev \
    libomxil-bellagio-dev \
    libosmesa6 \
    libosmesa6-dev \
    libpciaccess-dev \
    libpciaccess0 \
    libpng-dev \
    librust-bindgen-dev \
    libsensors5 \
    libssl-dev \
    libtinfo-dev \
    libtool \
    libudev-dev \
    libunwind-15 \
    libva-dev \
    libvdpau-dev \
    libvulkan-dev \
    libvulkan1 \
    libwayland-cursor0 \
    libwayland-egl-backend-dev \
    libwayland-egl1-mesa \
    libx11-dev \
    libx11-xcb-dev \
    libxatracker-dev \
    libxatracker2 \
    libxcb-dri2-0-dev \
    libxcb-dri3-dev \
    libxcb-glx0-dev \
    libxcb-present-dev \
    libxcb-randr0-dev \
    libxcb-shm0 \
    libxcb-shm0-dev \
    libxcb-sync-dev \
    libxcb-xfixes0-dev \
    libxdamage-dev \
    libxext-dev \
    libxfixes-dev \
    libxkbcommon0 \
    libxml2-dev \
    libxrandr-dev \
    libxrandr2 \
    libxrender1 \
    libxshmfence-dev \
    libxxf86vm-dev \
    libzstd-dev \
    libzstd1 \
    linux-libc-dev \
    llvm-15 \
    mesa-utils \
    meson \
    pkg-config \
    python3-mako \
    tar \
    valgrind

# LLVM-Config version fix
RUN rm /usr/bin/llvm-config && \
    ln -s /usr/bin/llvm-config-15 /usr/bin/llvm-config

# Ubuntu specific packages not all of the packages exist in repo
# it's extra step in build process but easier to maintain 
RUN apt-get install -y \
    libclang-15-dev \
    libclang-cpp15-dev \
    libllvmspirvlib-15-dev \ 
    libllvmspirvlib15 

# Pull mesa repository to /src directory
RUN git clone --depth 1 ${MESA_REPO} .


# Extand base image
FROM base as mesa_image
# RUN echo ${MESA_REPO}
RUN git checkout ${MESA_SHA} 

# setup, build, install mesa package    
RUN meson setup ${MESA_BUILD_PATH} \
    --prefix ${MESA_INSTALL_PATH} \
    -Dbuildtype=release \
    -Dvulkan-drivers=intel \
    -Dplatforms=x11 \
    -Dgallium-drivers=swrast,iris \
    -Dllvm=enabled && \
    ninja -C ${MESA_BUILD_PATH} && \
    ninja install -C ${MESA_BUILD_PATH}

# Package Mesa tar
RUN tar cvzf mesa_pkg_$(git rev-parse --short HEAD).tar.xz ${MESA_INSTALL_PATH}

# Adding Mount point
VOLUME /mesa

ENTRYPOINT cp -r /src/mesa_pkg_$(git rev-parse --short HEAD).tar.xz /mesa 